EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:USB3_A J1
U 1 1 606339C7
P 3000 1450
F 0 "J1" H 3057 2167 50  0000 C CNN
F 1 "USB3_A" H 3057 2076 50  0000 C CNN
F 2 "_Conectores:conn_usb3_xkb_vertical" H 3150 1550 50  0001 C CNN
F 3 "~" H 3150 1550 50  0001 C CNN
	1    3000 1450
	1    0    0    -1  
$EndComp
Text Label 1200 1100 2    50   ~ 0
IntA_P1_D+
Wire Wire Line
	1200 1100 1350 1100
Text Label 1200 1200 2    50   ~ 0
IntA_P1_D-
Wire Wire Line
	1200 1200 1350 1200
Text Label 1200 1300 2    50   ~ 0
GND
Wire Wire Line
	1200 1300 1350 1300
Text Label 1200 1400 2    50   ~ 0
IntA_P1_SSTX+
Wire Wire Line
	1200 1400 1350 1400
Text Label 1200 1500 2    50   ~ 0
IntA_P1_SSTX-
Wire Wire Line
	1200 1500 1350 1500
Text Label 1200 1600 2    50   ~ 0
GND
Wire Wire Line
	1200 1600 1350 1600
Text Label 1200 1700 2    50   ~ 0
IntA_P1_SSRX+
Wire Wire Line
	1200 1700 1350 1700
Text Label 1200 1800 2    50   ~ 0
IntA_P1_SSRX-
Wire Wire Line
	1200 1800 1350 1800
Text Label 1200 1900 2    50   ~ 0
Vbus
Wire Wire Line
	1200 1900 1350 1900
Text Label 2000 1800 0    50   ~ 0
Vbus
Wire Wire Line
	2000 1800 1850 1800
Text Label 2000 1700 0    50   ~ 0
IntA_P2_SSRX-
Wire Wire Line
	2000 1700 1850 1700
Text Label 2000 1600 0    50   ~ 0
IntA_P2_SSRX+
Wire Wire Line
	2000 1600 1850 1600
Text Label 2000 1500 0    50   ~ 0
GND
Wire Wire Line
	2000 1500 1850 1500
Text Label 2000 1400 0    50   ~ 0
IntA_P2_SSTX-
Wire Wire Line
	2000 1400 1850 1400
Text Label 2000 1300 0    50   ~ 0
IntA_P2_SSTX+
Wire Wire Line
	2000 1300 1850 1300
Text Label 2000 1200 0    50   ~ 0
GND
Wire Wire Line
	2000 1200 1850 1200
Text Label 2000 1100 0    50   ~ 0
IntA_P2_D-
Wire Wire Line
	2000 1100 1850 1100
Text Label 2000 1000 0    50   ~ 0
IntA_P2_D+
Wire Wire Line
	2000 1000 1850 1000
Text Label 3650 1350 0    50   ~ 0
IntA_P1_D+
Wire Wire Line
	3650 1350 3500 1350
Text Label 3650 1250 0    50   ~ 0
IntA_P1_D-
Wire Wire Line
	3650 1250 3500 1250
Text Label 3000 2300 0    50   ~ 0
GND
Wire Wire Line
	3000 2300 3000 2150
Text Label 3650 1950 0    50   ~ 0
IntA_P1_SSTX+
Wire Wire Line
	3650 1950 3500 1950
Text Label 3650 1850 0    50   ~ 0
IntA_P1_SSTX-
Wire Wire Line
	3650 1850 3500 1850
Text Label 3650 1650 0    50   ~ 0
IntA_P1_SSRX+
Wire Wire Line
	3650 1650 3500 1650
Text Label 3650 1550 0    50   ~ 0
IntA_P1_SSRX-
Wire Wire Line
	3650 1550 3500 1550
Text Label 3650 1050 0    50   ~ 0
Vbus
Wire Wire Line
	3650 1050 3500 1050
$Comp
L _Connector:Conn_2rows_19pins_USB3 conn1
U 1 1 606504CA
P 1600 1450
F 0 "conn1" H 1600 2165 50  0000 C CNN
F 1 "Conn_2rows_19pins_USB3" H 1600 2074 50  0000 C CNN
F 2 "_Conectores:usb3_pitch2.00mm-motherboard" H 1600 1250 50  0001 C CNN
F 3 "" H 1600 1250 50  0001 C CNN
	1    1600 1450
	1    0    0    1   
$EndComp
$Comp
L Connector:USB3_A J2
U 1 1 60653A17
P 3050 3100
F 0 "J2" H 3107 3817 50  0000 C CNN
F 1 "USB3_A" H 3107 3726 50  0000 C CNN
F 2 "_Conectores:conn_usb3_xkb_vertical" H 3200 3200 50  0001 C CNN
F 3 "~" H 3200 3200 50  0001 C CNN
	1    3050 3100
	1    0    0    -1  
$EndComp
Text Label 3700 2700 0    50   ~ 0
Vbus
Wire Wire Line
	3700 2700 3550 2700
Text Label 3700 3200 0    50   ~ 0
IntA_P2_SSRX-
Wire Wire Line
	3700 3300 3550 3300
Text Label 3700 3300 0    50   ~ 0
IntA_P2_SSRX+
Wire Wire Line
	3700 3200 3550 3200
Text Label 3700 3500 0    50   ~ 0
IntA_P2_SSTX-
Wire Wire Line
	3700 3600 3550 3600
Text Label 3700 3600 0    50   ~ 0
IntA_P2_SSTX+
Wire Wire Line
	3700 3500 3550 3500
Text Label 3050 3950 0    50   ~ 0
GND
Wire Wire Line
	3050 3950 3050 3800
Text Label 3700 2900 0    50   ~ 0
IntA_P2_D-
Wire Wire Line
	3700 3000 3550 3000
Text Label 3700 3000 0    50   ~ 0
IntA_P2_D+
Wire Wire Line
	3700 2900 3550 2900
$Comp
L Connector_Generic:Conn_01x04 conn2
U 1 1 60655044
P 750 6050
F 0 "conn2" H 668 5625 50  0000 C CNN
F 1 "Conn_01x04" H 668 5716 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 750 6050 50  0001 C CNN
F 3 "~" H 750 6050 50  0001 C CNN
	1    750  6050
	-1   0    0    1   
$EndComp
$Comp
L Connector:USB_B_Micro J7
U 1 1 60655F4D
P 1200 6650
F 0 "J7" H 1257 7117 50  0000 C CNN
F 1 "USB_B_Micro" H 1257 7026 50  0000 C CNN
F 2 "Connector_USB:USB_Mini-B_Tensility_54-00023_Vertical" H 1350 6600 50  0001 C CNN
F 3 "~" H 1350 6600 50  0001 C CNN
	1    1200 6650
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 conn3
U 1 1 6065B200
P 1300 6050
F 0 "conn3" H 1218 5625 50  0000 C CNN
F 1 "Conn_01x04" H 1218 5716 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 1300 6050 50  0001 C CNN
F 3 "~" H 1300 6050 50  0001 C CNN
	1    1300 6050
	-1   0    0    1   
$EndComp
$Comp
L Connector:Barrel_Jack_Switch conn4
U 1 1 6067252F
P 7500 1350
F 0 "conn4" H 7270 1300 50  0000 R CNN
F 1 "Barrel_Jack_Switch" H 7850 1600 50  0000 R CNN
F 2 "Connector_BarrelJack:BarrelJack_Horizontal" H 7550 1310 50  0001 C CNN
F 3 "~" H 7550 1310 50  0001 C CNN
	1    7500 1350
	-1   0    0    -1  
$EndComp
Entry Wire Line
	3600 6200 3700 6300
Entry Wire Line
	3600 6300 3700 6400
Entry Wire Line
	3600 6400 3700 6500
Entry Wire Line
	3600 6500 3700 6600
Entry Wire Line
	3600 6600 3700 6700
Entry Wire Line
	3600 6700 3700 6800
Entry Wire Line
	3600 6800 3700 6900
Entry Wire Line
	3600 6100 3700 6200
Wire Wire Line
	3550 6100 3600 6100
Wire Wire Line
	3550 6200 3600 6200
Wire Wire Line
	3550 6300 3600 6300
Wire Wire Line
	3550 6400 3600 6400
Wire Wire Line
	3550 6500 3600 6500
Wire Wire Line
	3550 6600 3600 6600
Wire Wire Line
	3550 6700 3600 6700
Wire Wire Line
	3550 6800 3600 6800
Entry Wire Line
	4050 5300 3950 5400
Entry Wire Line
	4050 5200 3950 5300
Entry Wire Line
	4050 5500 3950 5600
Entry Wire Line
	4050 5600 3950 5700
Entry Wire Line
	4050 6800 3950 6900
Entry Wire Line
	4050 6700 3950 6800
Entry Wire Line
	3950 7200 4050 7100
Entry Wire Line
	3950 7100 4050 7000
Text Label 1650 6450 0    50   ~ 0
Vbus
Wire Wire Line
	1650 6450 1500 6450
Text Label 4200 5100 2    50   ~ 0
Vbus
Text Label 4200 6600 2    50   ~ 0
Vbus
Text Label 1650 6150 0    50   ~ 0
Vbus
Wire Wire Line
	1650 6150 1500 6150
Wire Wire Line
	1650 6050 1500 6050
Wire Wire Line
	1650 5950 1500 5950
Wire Wire Line
	1650 5850 1500 5850
Text Label 1650 6050 0    50   ~ 0
DMU
Text Label 1650 5950 0    50   ~ 0
DPU
Text Label 1650 5850 0    50   ~ 0
GND
Wire Wire Line
	2350 7100 2500 7100
Wire Wire Line
	2350 7000 2500 7000
Text Label 2450 7100 2    50   ~ 0
DMU
Text Label 2450 7000 2    50   ~ 0
DPU
Wire Wire Line
	1650 6750 1500 6750
Wire Wire Line
	1650 6650 1500 6650
Text Label 1650 6750 0    50   ~ 0
DMU
Text Label 1650 6650 0    50   ~ 0
DPU
NoConn ~ 1500 6850
Text Label 1100 6150 2    50   ~ 0
Vbus
Wire Wire Line
	1100 6150 950  6150
Wire Wire Line
	1100 6050 950  6050
Wire Wire Line
	1100 5950 950  5950
Wire Wire Line
	1100 5850 950  5850
Text Label 1100 5850 2    50   ~ 0
GND
Text Label 1100 6050 2    50   ~ 0
D+
Text Label 1100 5950 2    50   ~ 0
D-
Text Label 1200 7150 0    50   ~ 0
GND
Wire Wire Line
	1200 7050 1200 7150
Wire Wire Line
	2900 2150 2900 2300
Wire Wire Line
	2900 2300 3000 2300
Wire Wire Line
	2800 2150 2800 2300
Wire Wire Line
	2800 2300 2900 2300
Connection ~ 2900 2300
Wire Wire Line
	1100 7050 1100 7150
Text Label 4050 5400 0    50   ~ 0
GND
Text Label 3550 6100 0    50   ~ 0
D4-
Text Label 3550 6200 0    50   ~ 0
D4+
Text Label 3550 6300 0    50   ~ 0
D3-
Text Label 3550 6500 0    50   ~ 0
D2-
Text Label 3550 6700 0    50   ~ 0
D1-
Text Label 3550 6400 0    50   ~ 0
D3+
Text Label 3550 6600 0    50   ~ 0
D2+
Text Label 3550 6800 0    50   ~ 0
D1+
Text Label 4200 7100 2    50   ~ 0
D4-
Text Label 4200 7000 2    50   ~ 0
D4+
Text Label 4200 6800 2    50   ~ 0
D3-
Text Label 4050 5600 0    50   ~ 0
D2-
Text Label 4200 6700 2    50   ~ 0
D3+
Text Label 4200 5500 2    50   ~ 0
D2+
Text Label 4050 5200 0    50   ~ 0
D1+
Text Label 4050 5300 0    50   ~ 0
D1-
$Comp
L power:VDC #PWR05
U 1 1 607F1977
P 6700 1050
F 0 "#PWR05" H 6700 950 50  0001 C CNN
F 1 "VDC" H 6700 1325 50  0000 C CNN
F 2 "" H 6700 1050 50  0001 C CNN
F 3 "" H 6700 1050 50  0001 C CNN
	1    6700 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 1050 6700 1250
Wire Wire Line
	6700 1250 7200 1250
Wire Wire Line
	7200 1350 6700 1350
Wire Wire Line
	6700 1350 6700 1450
Wire Wire Line
	7200 1450 6700 1450
Connection ~ 6700 1450
Wire Wire Line
	6700 1450 6700 1750
$Comp
L power:GND #PWR06
U 1 1 607FB98A
P 6700 1750
F 0 "#PWR06" H 6700 1500 50  0001 C CNN
F 1 "GND" H 6705 1577 50  0000 C CNN
F 2 "" H 6700 1750 50  0001 C CNN
F 3 "" H 6700 1750 50  0001 C CNN
	1    6700 1750
	1    0    0    -1  
$EndComp
Text Label 6700 1450 0    50   ~ 0
GND
Wire Wire Line
	2850 3800 2850 3950
Wire Wire Line
	2850 3950 2950 3950
Wire Wire Line
	2950 3800 2950 3950
Connection ~ 2950 3950
Wire Wire Line
	2950 3950 3050 3950
$Comp
L Device:Jumper JP1
U 1 1 608A1C6C
P 1550 2800
F 0 "JP1" H 1550 3064 50  0000 C CNN
F 1 "Jumper" H 1550 2973 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1550 2800 50  0001 C CNN
F 3 "~" H 1550 2800 50  0001 C CNN
	1    1550 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP2
U 1 1 608A2B6C
P 1550 3100
F 0 "JP2" H 1550 3364 50  0000 C CNN
F 1 "Jumper" H 1550 3273 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1550 3100 50  0001 C CNN
F 3 "~" H 1550 3100 50  0001 C CNN
	1    1550 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP3
U 1 1 608A5C03
P 1550 3350
F 0 "JP3" H 1550 3614 50  0000 C CNN
F 1 "Jumper" H 1550 3523 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1550 3350 50  0001 C CNN
F 3 "~" H 1550 3350 50  0001 C CNN
	1    1550 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP4
U 1 1 608A5C09
P 1550 3650
F 0 "JP4" H 1550 3914 50  0000 C CNN
F 1 "Jumper" H 1550 3823 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1550 3650 50  0001 C CNN
F 3 "~" H 1550 3650 50  0001 C CNN
	1    1550 3650
	1    0    0    -1  
$EndComp
Text Label 1250 2800 2    50   ~ 0
IntA_P2_D-
Text Label 1250 3100 2    50   ~ 0
IntA_P2_D+
Text Label 1250 3650 2    50   ~ 0
IntA_P1_D+
Text Label 1250 3350 2    50   ~ 0
IntA_P1_D-
Text Label 1850 3350 0    50   ~ 0
D+
Text Label 1850 3650 0    50   ~ 0
D-
Text Label 1850 3100 0    50   ~ 0
DMU
Text Label 1850 2800 0    50   ~ 0
DPU
Entry Wire Line
	2250 7100 2350 7000
Entry Wire Line
	1650 6450 1750 6550
Entry Wire Line
	1650 6650 1750 6750
Entry Wire Line
	1650 6750 1750 6850
Entry Wire Line
	2250 7200 2350 7100
Entry Wire Line
	1650 5850 1750 5950
Entry Wire Line
	1650 5950 1750 6050
Entry Wire Line
	1650 6050 1750 6150
Entry Wire Line
	1650 6150 1750 6250
Entry Bus Bus
	1750 7300 1850 7400
Entry Bus Bus
	2150 7400 2250 7300
Entry Bus Bus
	3700 7300 3600 7400
Entry Wire Line
	1650 7150 1750 7250
Entry Wire Line
	4050 6600 3950 6700
Entry Wire Line
	4050 5100 3950 5200
Entry Wire Line
	4050 6900 3950 7000
Entry Wire Line
	4050 5400 3950 5500
Wire Wire Line
	4050 5400 4600 5400
Wire Wire Line
	1100 7150 1200 7150
Wire Wire Line
	1650 7150 1200 7150
Connection ~ 1200 7150
Text Label 3550 5800 0    50   ~ 0
GND
Text Label 3550 5900 0    50   ~ 0
XTAL+
Text Label 3550 6000 0    50   ~ 0
XTAL-
Text Label 2500 5800 2    50   ~ 0
VD18
Text Label 3550 6900 0    50   ~ 0
VD18
Text Label 3550 7000 0    50   ~ 0
VD33
$Comp
L power:VDC #PWR04
U 1 1 609AAB67
P 6200 1000
F 0 "#PWR04" H 6200 900 50  0001 C CNN
F 1 "VDC" H 6200 1275 50  0000 C CNN
F 2 "" H 6200 1000 50  0001 C CNN
F 3 "" H 6200 1000 50  0001 C CNN
	1    6200 1000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 609AC3EC
P 5700 2000
F 0 "#PWR03" H 5700 1750 50  0001 C CNN
F 1 "GND" H 5705 1827 50  0000 C CNN
F 2 "" H 5700 2000 50  0001 C CNN
F 3 "" H 5700 2000 50  0001 C CNN
	1    5700 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 609ACCE3
P 6200 1600
F 0 "C2" H 6318 1646 50  0000 L CNN
F 1 "10uF" H 6318 1555 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A_Pad1.58x1.35mm_HandSolder" H 6238 1450 50  0001 C CNN
F 3 "~" H 6200 1600 50  0001 C CNN
	1    6200 1600
	1    0    0    -1  
$EndComp
Text Label 5150 1300 2    50   ~ 0
Vbus
$Comp
L power:VBUS #PWR02
U 1 1 609E2B99
P 5150 900
F 0 "#PWR02" H 5150 750 50  0001 C CNN
F 1 "VBUS" H 5165 1073 50  0000 C CNN
F 2 "" H 5150 900 50  0001 C CNN
F 3 "" H 5150 900 50  0001 C CNN
	1    5150 900 
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:LD1086D2T33TR U2
U 1 1 609E86BA
P 5700 1300
F 0 "U2" H 5700 1667 50  0000 C CNN
F 1 "LD1086D2T5TR" H 5700 1576 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-263-2" H 5700 1800 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/ld1086.pdf" H 5700 1800 50  0001 C CNN
	1    5700 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C1
U 1 1 609FD6E6
P 5150 1600
F 0 "C1" H 5268 1646 50  0000 L CNN
F 1 "10uF" H 5268 1555 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A_Pad1.58x1.35mm_HandSolder" H 5188 1450 50  0001 C CNN
F 3 "~" H 5150 1600 50  0001 C CNN
	1    5150 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 900  5150 1300
Wire Wire Line
	5150 1300 5300 1300
Connection ~ 5150 1300
Wire Wire Line
	5150 1300 5150 1450
Wire Wire Line
	6200 1000 6200 1300
Wire Wire Line
	6200 1300 6100 1300
Wire Wire Line
	6200 1300 6200 1450
Connection ~ 6200 1300
Wire Wire Line
	5150 1850 5700 1850
Wire Wire Line
	6200 1850 6200 1750
Wire Wire Line
	5150 1750 5150 1850
Wire Wire Line
	5700 1600 5700 1850
Connection ~ 5700 1850
Wire Wire Line
	5700 1850 6200 1850
Wire Wire Line
	5700 1850 5700 2000
$Comp
L power:VBUS #PWR01
U 1 1 60A1EC95
P 4550 900
F 0 "#PWR01" H 4550 750 50  0001 C CNN
F 1 "VBUS" H 4565 1073 50  0000 C CNN
F 2 "" H 4550 900 50  0001 C CNN
F 3 "" H 4550 900 50  0001 C CNN
	1    4550 900 
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 60A1F7BC
P 4550 1150
F 0 "R1" H 4620 1196 50  0000 L CNN
F 1 "R" H 4620 1105 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4480 1150 50  0001 C CNN
F 3 "~" H 4550 1150 50  0001 C CNN
	1    4550 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 60A1FD08
P 4550 1550
F 0 "D1" V 4589 1433 50  0000 R CNN
F 1 "LED" V 4498 1433 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4550 1550 50  0001 C CNN
F 3 "~" H 4550 1550 50  0001 C CNN
	1    4550 1550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4550 900  4550 1000
Wire Wire Line
	4550 1300 4550 1400
Wire Wire Line
	4550 1700 4550 1850
Wire Wire Line
	4550 1850 5150 1850
Connection ~ 5150 1850
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J9
U 1 1 60A41056
P 10050 1100
F 0 "J9" H 10100 1517 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 10100 1426 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x06_P2.54mm_Vertical" H 10050 1100 50  0001 C CNN
F 3 "~" H 10050 1100 50  0001 C CNN
	1    10050 1100
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J3
U 1 1 60A4F5D3
P 10200 3250
F 0 "J3" H 10250 3667 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 10250 3576 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x06_P2.54mm_Vertical" H 10200 3250 50  0001 C CNN
F 3 "~" H 10200 3250 50  0001 C CNN
	1    10200 3250
	1    0    0    -1  
$EndComp
$Comp
L _Connector:CONN_USB_DUAL Conn_USB_Dual1
U 1 1 60A75FC4
P 5000 4850
F 0 "Conn_USB_Dual1" H 5000 5265 50  0000 C CNN
F 1 "CONN_USB_DUAL" H 5000 5174 50  0000 C CNN
F 2 "_Conectores:conn_2_usb2_separed_pins" H 5150 4800 50  0001 C CNN
F 3 "" H 5150 4800 50  0001 C CNN
	1    5000 4850
	-1   0    0    -1  
$EndComp
$Comp
L _Connector:CONN_USB_DUAL Conn_USB_Dual2
U 1 1 60A77B9A
P 5000 6350
F 0 "Conn_USB_Dual2" H 5000 6765 50  0000 C CNN
F 1 "CONN_USB_DUAL" H 5000 6674 50  0000 C CNN
F 2 "_Conectores:conn_2_usb2_separed_pins" H 5150 6300 50  0001 C CNN
F 3 "" H 5150 6300 50  0001 C CNN
	1    5000 6350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4650 5000 4600 5000
Wire Wire Line
	4600 5000 4600 5400
Connection ~ 4600 5400
Wire Wire Line
	5000 5400 5000 5350
Wire Wire Line
	4600 5400 5000 5400
Wire Wire Line
	5000 5400 5400 5400
Wire Wire Line
	5400 5400 5400 5000
Wire Wire Line
	5400 5000 5350 5000
Connection ~ 5000 5400
Wire Wire Line
	4500 5300 4500 4900
Wire Wire Line
	4500 4900 4650 4900
Wire Wire Line
	4050 5300 4500 5300
Wire Wire Line
	4400 5200 4400 4800
Wire Wire Line
	4400 4800 4650 4800
Wire Wire Line
	4050 5200 4400 5200
Wire Wire Line
	4050 5100 4300 5100
Wire Wire Line
	4300 4700 4600 4700
Wire Wire Line
	4600 4350 5450 4350
Wire Wire Line
	5450 4700 5350 4700
Connection ~ 4600 4700
Wire Wire Line
	4600 4700 4650 4700
Wire Wire Line
	5350 4900 5500 4900
Wire Wire Line
	5500 4900 5500 5500
Wire Wire Line
	4050 5500 5500 5500
Wire Wire Line
	5600 5600 5600 4800
Wire Wire Line
	5600 4800 5350 4800
Wire Wire Line
	4050 5600 5600 5600
Wire Wire Line
	4050 6900 4600 6900
Wire Wire Line
	4600 6900 4600 6500
Wire Wire Line
	4600 6500 4650 6500
Wire Wire Line
	4600 6900 5000 6900
Wire Wire Line
	5000 6900 5000 6850
Connection ~ 4600 6900
Wire Wire Line
	5000 6900 5450 6900
Wire Wire Line
	5450 6900 5450 6500
Wire Wire Line
	5450 6500 5350 6500
Connection ~ 5000 6900
Wire Wire Line
	4500 6800 4500 6400
Wire Wire Line
	4500 6400 4650 6400
Wire Wire Line
	4050 6800 4500 6800
Wire Wire Line
	4400 6700 4400 6300
Wire Wire Line
	4400 6300 4650 6300
Wire Wire Line
	4050 6700 4400 6700
Wire Wire Line
	4300 6600 4300 6200
Wire Wire Line
	4300 6200 4600 6200
Wire Wire Line
	4050 6600 4300 6600
Wire Wire Line
	4600 6200 4600 5850
Wire Wire Line
	4600 5850 5450 5850
Wire Wire Line
	5450 5850 5450 6200
Wire Wire Line
	5450 6200 5350 6200
Connection ~ 4600 6200
Wire Wire Line
	4600 6200 4650 6200
Wire Wire Line
	5550 7000 5550 6400
Wire Wire Line
	5550 6400 5350 6400
Wire Wire Line
	4050 7000 5550 7000
Wire Wire Line
	5650 7100 5650 6300
Wire Wire Line
	5650 6300 5350 6300
Wire Wire Line
	4050 7100 5650 7100
Text Label 4050 6900 0    50   ~ 0
GND
Wire Wire Line
	4300 4700 4300 5100
Entry Bus Bus
	3950 7300 3850 7400
Wire Wire Line
	4600 4350 4600 4700
Wire Wire Line
	5450 4350 5450 4700
Text Label 3550 7100 0    50   ~ 0
REXT
Text Label 2500 6000 2    50   ~ 0
0VCJ
Text Label 2500 6200 2    50   ~ 0
LED2
Text Label 2500 6300 2    50   ~ 0
LED1
Text Label 2500 6400 2    50   ~ 0
DRV
Text Label 2500 6500 2    50   ~ 0
VDD33
Text Label 2500 6600 2    50   ~ 0
VDD5
Text Label 1200 5000 0    50   ~ 0
VBUSM
Text Label 2500 6900 2    50   ~ 0
XRSTJ
Wire Notes Line
	6000 4000 6000 7650
Wire Notes Line
	6000 7650 550  7650
Wire Notes Line
	550  7650 550  550 
Text Notes 5850 7550 2    100  Italic 20
USB 2 HUB PORTS
Wire Notes Line
	550  4350 4350 4350
Wire Notes Line
	550  2350 2350 2350
Wire Notes Line
	2350 2350 2350 4350
Text Notes 2300 4250 2    100  Italic 20
USB 2 -> 3 Bridge 
Text Notes 4350 4250 2    100  Italic 20
USB 3 / 2 Direct PORTS
Wire Notes Line
	6000 7300 4300 7300
Wire Notes Line
	4300 7300 4300 7650
Wire Notes Line
	8250 2400 8250 550 
Wire Notes Line
	6350 2100 6350 2400
Text Notes 8300 2300 2    100  Italic 20
Power Input 5V to 30 V 
$Comp
L Device:C C5
U 1 1 60DC2A70
P 1850 4650
F 0 "C5" V 1598 4650 50  0000 C CNN
F 1 "10uF" V 1689 4650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1888 4500 50  0001 C CNN
F 3 "~" H 1850 4650 50  0001 C CNN
	1    1850 4650
	0    1    1    0   
$EndComp
Text Label 850  4650 2    50   ~ 0
REXT
$Comp
L Device:R R2
U 1 1 60DD4009
P 1000 4650
F 0 "R2" V 950 4500 50  0000 C CNN
F 1 "2,7K" V 1000 4650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 930 4650 50  0001 C CNN
F 3 "~" H 1000 4650 50  0001 C CNN
	1    1000 4650
	0    -1   -1   0   
$EndComp
Text Label 1150 4650 0    50   ~ 0
GND
Text Label 1700 4650 2    50   ~ 0
VD18
Text Label 2000 4650 0    50   ~ 0
GND
Text Label 2350 4650 2    50   ~ 0
VD33
Text Label 2650 4650 0    50   ~ 0
GND
$Comp
L Device:C C6
U 1 1 60DDD4F5
P 2500 4650
F 0 "C6" V 2752 4650 50  0000 C CNN
F 1 "10uF" V 2661 4650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2538 4500 50  0001 C CNN
F 3 "~" H 2500 4650 50  0001 C CNN
	1    2500 4650
	0    1    -1   0   
$EndComp
NoConn ~ 2500 5900
NoConn ~ 2500 6100
Text Label 2650 4800 0    50   ~ 0
0VCJ
Text Label 2650 4900 0    50   ~ 0
VDD33
Text Label 2000 5300 0    50   ~ 0
XTAL+
Text Label 1700 5300 2    50   ~ 0
XTAL-
Wire Wire Line
	2350 4650 2350 4800
Wire Wire Line
	2350 4800 2650 4800
Wire Wire Line
	2650 4900 2350 4900
Connection ~ 2350 4800
$Comp
L Device:R R5
U 1 1 60E19DB4
P 2500 5000
F 0 "R5" V 2450 4850 50  0000 C CNN
F 1 "100k" V 2500 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2430 5000 50  0001 C CNN
F 3 "~" H 2500 5000 50  0001 C CNN
	1    2500 5000
	0    -1   -1   0   
$EndComp
Connection ~ 2350 4900
$Comp
L Device:R R6
U 1 1 60E20EFC
P 2500 5150
F 0 "R6" V 2450 5000 50  0000 C CNN
F 1 "100k" V 2500 5150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2430 5150 50  0001 C CNN
F 3 "~" H 2500 5150 50  0001 C CNN
	1    2500 5150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2350 5000 2350 5150
Connection ~ 2350 5000
Text Label 2650 5000 0    50   ~ 0
XRSTJ
$Comp
L _CI:fe1.1s U1
U 1 1 60E2A516
P 3100 6450
F 0 "U1" H 3175 7365 50  0000 C CNN
F 1 "fe1.1s" H 3175 7274 50  0000 C CNN
F 2 "Package_SO:TSSOP-28_4.4x9.7mm_P0.65mm" H 3100 6450 50  0001 C CNN
F 3 "" H 3100 6450 50  0001 C CNN
	1    3100 6450
	-1   0    0    -1  
$EndComp
Text Label 2500 6700 2    50   ~ 0
BUSJ
Text Label 2650 5150 0    50   ~ 0
BUSJ
$Comp
L Device:Crystal Y1
U 1 1 60E370D3
P 1850 5300
F 0 "Y1" H 1850 5568 50  0000 C CNN
F 1 "Crystal" H 1850 5477 50  0000 C CNN
F 2 "Crystal:Crystal_DS10_D1.0mm_L4.3mm_Vertical" H 1850 5300 50  0001 C CNN
F 3 "~" H 1850 5300 50  0001 C CNN
	1    1850 5300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 60E40FF2
P 1700 5450
F 0 "C7" H 1900 5400 50  0000 R CNN
F 1 "20pF" H 2000 5500 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1738 5300 50  0001 C CNN
F 3 "~" H 1700 5450 50  0001 C CNN
	1    1700 5450
	-1   0    0    1   
$EndComp
$Comp
L Device:C C8
U 1 1 60E416B0
P 2000 5450
F 0 "C8" H 1885 5404 50  0000 R CNN
F 1 "20pF" H 1885 5495 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2038 5300 50  0001 C CNN
F 3 "~" H 2000 5450 50  0001 C CNN
	1    2000 5450
	-1   0    0    1   
$EndComp
Text Label 1700 5600 0    50   ~ 0
GND
Text Label 2000 5600 0    50   ~ 0
GND
Text Label 1200 4900 0    50   ~ 0
VDD5
Text Label 850  4900 2    50   ~ 0
Vbus
Wire Wire Line
	850  4900 1200 4900
$Comp
L Device:R R3
U 1 1 60E65218
P 1050 5000
F 0 "R3" V 1000 4850 50  0000 C CNN
F 1 "47k" V 1050 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 980 5000 50  0001 C CNN
F 3 "~" H 1050 5000 50  0001 C CNN
	1    1050 5000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	850  4900 850  5000
Wire Wire Line
	850  5000 900  5000
Text Label 1700 4850 2    50   ~ 0
VBUSM
$Comp
L Device:R R4
U 1 1 60E7BB7D
P 1850 4850
F 0 "R4" V 1900 5000 50  0000 C CNN
F 1 "100k" V 1850 4850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1780 4850 50  0001 C CNN
F 3 "~" H 1850 4850 50  0001 C CNN
	1    1850 4850
	0    1    1    0   
$EndComp
Text Label 2000 4850 0    50   ~ 0
GND
$Comp
L Device:C C3
U 1 1 60E8B9BE
P 1050 5200
F 0 "C3" V 1100 5400 50  0000 R CNN
F 1 "20pF" V 1150 5150 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1088 5050 50  0001 C CNN
F 3 "~" H 1050 5200 50  0001 C CNN
	1    1050 5200
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C4
U 1 1 60E8BEFD
P 1050 5400
F 0 "C4" V 1100 5600 50  0000 R CNN
F 1 "20pF" V 1150 5350 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1088 5250 50  0001 C CNN
F 3 "~" H 1050 5400 50  0001 C CNN
	1    1050 5400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	850  5000 850  5200
Wire Wire Line
	850  5200 900  5200
Connection ~ 850  5000
Wire Wire Line
	850  5200 850  5400
Wire Wire Line
	850  5400 900  5400
Connection ~ 850  5200
Text Label 1200 5200 0    50   ~ 0
GND
Text Label 1200 5400 0    50   ~ 0
GND
$Comp
L Device:LED D2
U 1 1 60EA963B
P 3150 4700
F 0 "D2" V 3350 4750 50  0000 R CNN
F 1 "LED" V 3098 4583 50  0001 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3150 4700 50  0001 C CNN
F 3 "~" H 3150 4700 50  0001 C CNN
	1    3150 4700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2350 4900 2350 5000
Wire Wire Line
	2350 4800 2350 4900
$Comp
L Device:LED D3
U 1 1 60EE7A52
P 3450 4700
F 0 "D3" V 3650 4750 50  0000 R CNN
F 1 "LED" V 3398 4583 50  0001 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3450 4700 50  0001 C CNN
F 3 "~" H 3450 4700 50  0001 C CNN
	1    3450 4700
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D4
U 1 1 60EEF227
P 3600 4700
F 0 "D4" V 3400 4650 50  0000 L CNN
F 1 "LED" V 3638 4778 50  0001 L CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3600 4700 50  0001 C CNN
F 3 "~" H 3600 4700 50  0001 C CNN
	1    3600 4700
	0    1    1    0   
$EndComp
$Comp
L Device:LED D5
U 1 1 60EF16DC
P 3750 4700
F 0 "D5" V 3950 4750 50  0000 R CNN
F 1 "LED" V 3698 4583 50  0001 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3750 4700 50  0001 C CNN
F 3 "~" H 3750 4700 50  0001 C CNN
	1    3750 4700
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D6
U 1 1 60EF16E2
P 3900 4700
F 0 "D6" V 3700 4650 50  0000 L CNN
F 1 "LED" V 3938 4778 50  0001 L CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3900 4700 50  0001 C CNN
F 3 "~" H 3900 4700 50  0001 C CNN
	1    3900 4700
	0    1    1    0   
$EndComp
Wire Wire Line
	3150 4550 3450 4550
Connection ~ 3450 4550
Connection ~ 3600 4550
Connection ~ 3750 4550
Wire Wire Line
	3750 4550 3900 4550
Text Label 3350 4550 2    50   ~ 0
DRV
$Comp
L Device:R R7
U 1 1 60F0334E
P 3150 5000
F 0 "R7" V 3100 4850 50  0000 C CNN
F 1 "330" V 3150 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3080 5000 50  0001 C CNN
F 3 "~" H 3150 5000 50  0001 C CNN
	1    3150 5000
	1    0    0    -1  
$EndComp
Text Label 3150 5150 3    50   ~ 0
GND
$Comp
L Device:R R8
U 1 1 60F0434A
P 3450 5000
F 0 "R8" V 3400 4850 50  0000 C CNN
F 1 "330" V 3450 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3380 5000 50  0001 C CNN
F 3 "~" H 3450 5000 50  0001 C CNN
	1    3450 5000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 60F04762
P 3750 5000
F 0 "R9" V 3700 4850 50  0000 C CNN
F 1 "330" V 3750 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3680 5000 50  0001 C CNN
F 3 "~" H 3750 5000 50  0001 C CNN
	1    3750 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 4850 3450 4850
Connection ~ 3450 4850
Wire Wire Line
	3900 4850 3750 4850
Connection ~ 3750 4850
Text Label 3450 5150 3    50   ~ 0
LED2
Text Label 3750 5150 3    50   ~ 0
LED1
Wire Wire Line
	3600 4550 3750 4550
Wire Wire Line
	3450 4550 3600 4550
Text Label 2500 6800 2    50   ~ 0
VBUSM
Text Label 9500 900  0    50   ~ 0
hddLed+
Text Label 9500 1000 0    50   ~ 0
hddLed-
Text Label 9500 1100 0    50   ~ 0
pwr+
Text Label 9500 1200 0    50   ~ 0
pwr-
Text Label 10750 900  2    50   ~ 0
pwrLed+
Text Label 10750 1000 2    50   ~ 0
pwrLed-
Wire Wire Line
	9500 900  9850 900 
Wire Wire Line
	9500 1000 9850 1000
Wire Wire Line
	9500 1100 9850 1100
Wire Wire Line
	9500 1200 9850 1200
Wire Wire Line
	10350 900  10750 900 
Wire Wire Line
	10750 1000 10350 1000
Wire Wire Line
	10750 1100 10350 1100
Wire Wire Line
	10750 1200 10350 1200
Wire Wire Line
	10750 1300 10350 1300
Wire Wire Line
	9500 1300 9850 1300
Text Label 10750 1100 2    50   ~ 0
rst+
Text Label 10750 1200 2    50   ~ 0
rst-
Text Label 9500 1300 0    50   ~ 0
Speaker+
Text Label 10750 1300 2    50   ~ 0
Speaker-
Text Label 10000 3050 2    50   ~ 0
mic
Text Label 10500 3050 0    50   ~ 0
aud_gnd
Text Label 10000 3150 2    50   ~ 0
mic_bias
Text Label 10000 3250 2    50   ~ 0
fp_out_R
Text Label 10500 3150 0    50   ~ 0
aud_gnd
Text Label 10500 3250 0    50   ~ 0
fp_ret_R
Text Label 10000 3350 2    50   ~ 0
aud_5v
NoConn ~ 10500 3350
Text Label 10000 3450 2    50   ~ 0
fp_out_L
Text Label 10500 3450 0    50   ~ 0
fp_ret_L
$Comp
L _Connector:Audio_Jack2_PJ-3900 audConn1
U 1 1 60984D87
P 8700 2950
F 0 "audConn1" H 8258 3225 50  0000 C CNN
F 1 "Audio_Jack2_PJ-3900" H 8258 3134 50  0000 C CNN
F 2 "_Conectores:audioJack_PJ3900" H 8700 2950 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1810121713_Korean-Hroparts-Elec-PJ-3900-04_C151839.pdf" H 8700 2950 50  0001 C CNN
	1    8700 2950
	1    0    0    -1  
$EndComp
$Comp
L _Connector:Audio_Jack2_PJ-3900 audConn2
U 1 1 609856F9
P 6600 2950
F 0 "audConn2" H 6158 3225 50  0000 C CNN
F 1 "Audio_Jack2_PJ-3900" H 6158 3134 50  0000 C CNN
F 2 "_Conectores:audioJack_PJ3900" H 6600 2950 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1810121713_Korean-Hroparts-Elec-PJ-3900-04_C151839.pdf" H 6600 2950 50  0001 C CNN
	1    6600 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:Speaker LS1
U 1 1 60986E8C
P 9150 1850
F 0 "LS1" H 9113 1433 50  0000 C CNN
F 1 "Speaker" H 9113 1524 50  0000 C CNN
F 2 "Buzzer_Beeper:Buzzer_12x9.5RM7.6" H 9150 1650 50  0001 C CNN
F 3 "~" H 9140 1800 50  0001 C CNN
	1    9150 1850
	-1   0    0    1   
$EndComp
Text Label 9350 1750 0    50   ~ 0
Speaker+
Text Label 9350 1850 0    50   ~ 0
Speaker-
Text Label 8750 900  0    50   ~ 0
hddLed+
Text Label 8750 1050 0    50   ~ 0
hddLed-
Text Label 8750 1250 0    50   ~ 0
pwrLed+
Text Label 8750 1400 0    50   ~ 0
pwrLed-
Text Label 10600 1750 0    50   ~ 0
rst+
Text Label 10600 1850 0    50   ~ 0
rst-
Text Label 10050 1750 0    50   ~ 0
pwr+
Text Label 10050 1850 0    50   ~ 0
pwr-
$Comp
L Device:LED D7
U 1 1 609D3249
P 8600 900
F 0 "D7" H 8593 1116 50  0000 C CNN
F 1 "LED" H 8593 1025 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 8600 900 50  0001 C CNN
F 3 "~" H 8600 900 50  0001 C CNN
	1    8600 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 900  8450 1050
Wire Wire Line
	8450 1050 8750 1050
$Comp
L Device:LED D8
U 1 1 609DDA55
P 8600 1250
F 0 "D8" H 8593 1466 50  0000 C CNN
F 1 "LED" H 8593 1375 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 8600 1250 50  0001 C CNN
F 3 "~" H 8600 1250 50  0001 C CNN
	1    8600 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 1250 8450 1400
Wire Wire Line
	8450 1400 8750 1400
$Comp
L Connector_Generic:Conn_01x02 J4
U 1 1 609FD85A
P 10400 1750
F 0 "J4" H 10318 1967 50  0000 C CNN
F 1 "Conn_01x02" H 10318 1876 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_6x6mm_H9.5mm" H 10400 1750 50  0001 C CNN
F 3 "~" H 10400 1750 50  0001 C CNN
	1    10400 1750
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J5
U 1 1 609FF435
P 9850 1750
F 0 "J5" H 9768 1967 50  0000 C CNN
F 1 "Conn_01x02" H 9768 1876 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_6x6mm_H9.5mm" H 9850 1750 50  0001 C CNN
F 3 "~" H 9850 1750 50  0001 C CNN
	1    9850 1750
	-1   0    0    -1  
$EndComp
Text Label 8800 3000 0    50   ~ 0
mic
Text Label 8800 3100 0    50   ~ 0
mic
Text Label 8800 3200 0    50   ~ 0
mic_bias
NoConn ~ 8800 3300
Text Label 8800 2900 0    50   ~ 0
aud_gnd
Text Label 6700 2900 0    50   ~ 0
aud_gnd
Text Label 6700 3000 0    50   ~ 0
fp_out_L
Text Label 6700 3100 0    50   ~ 0
fp_out_L
Text Label 6700 3200 0    50   ~ 0
fp_ret_R
Text Label 7050 3200 0    50   ~ 0
fp_ret_L
Wire Wire Line
	7050 3200 6700 3200
NoConn ~ 6700 3300
NoConn ~ 1350 1000
Wire Notes Line
	6350 2100 8250 2100
Text Notes 11200 2350 2    100  Italic 20
MotherBoard pinouts
Wire Notes Line
	9500 2400 9500 2100
Wire Notes Line
	9500 2100 11200 2100
Wire Notes Line
	4400 2400 11200 2400
Wire Notes Line
	4350 4350 4350 4000
Wire Notes Line
	550  550  11200 550 
Wire Notes Line
	4400 550  4400 4000
Wire Notes Line
	550  4000 11200 4000
Wire Notes Line
	11200 550  11200 4000
Text Notes 11150 3950 2    100  Italic 20
Audio Frontal  pinouts
Wire Notes Line
	11200 3750 9300 3750
Wire Notes Line
	9300 3750 9300 4000
Wire Bus Line
	2250 7100 2250 7300
Wire Bus Line
	1850 7400 3850 7400
Wire Bus Line
	1750 5950 1750 7300
Wire Bus Line
	3700 6200 3700 7300
Wire Bus Line
	3950 5200 3950 7300
$Comp
L Mechanical:MountingHole H1
U 1 1 60B23D4C
P 7950 4450
F 0 "H1" H 8050 4496 50  0000 L CNN
F 1 "Hole" H 8050 4405 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 7950 4450 50  0001 C CNN
F 3 "~" H 7950 4450 50  0001 C CNN
	1    7950 4450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 60B24E94
P 8350 4450
F 0 "H2" H 8450 4496 50  0000 L CNN
F 1 "Hole" H 8450 4405 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 8350 4450 50  0001 C CNN
F 3 "~" H 8350 4450 50  0001 C CNN
	1    8350 4450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 60B2507B
P 8700 4450
F 0 "H3" H 8800 4496 50  0000 L CNN
F 1 "Hole" H 8800 4405 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 8700 4450 50  0001 C CNN
F 3 "~" H 8700 4450 50  0001 C CNN
	1    8700 4450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 60B252B8
P 9100 4450
F 0 "H4" H 9200 4496 50  0000 L CNN
F 1 "Hole" H 9200 4405 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 9100 4450 50  0001 C CNN
F 3 "~" H 9100 4450 50  0001 C CNN
	1    9100 4450
	1    0    0    -1  
$EndComp
$EndSCHEMATC
